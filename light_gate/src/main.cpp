#include <Arduino.h>

#define potPin 4
#define ledPin 23

int val = 0;

void setup()
{
  Serial.begin(115200);
  delay(1000);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

void loop()
{
  val = analogRead(potPin);
  if (val > 4000)
  {
    digitalWrite(ledPin, LOW);
  }
  else
  {
    digitalWrite(ledPin, HIGH);
  }
  Serial.println(val);
  delay(1);
}